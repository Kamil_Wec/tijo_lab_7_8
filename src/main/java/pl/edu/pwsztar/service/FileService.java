package pl.edu.pwsztar.service;

import org.springframework.core.io.InputStreamResource;
import pl.edu.pwsztar.domain.dto.FileDto;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface FileService{
    List<FileDto> getSortedMoviesList();
    InputStreamResource getInputStream(File file) throws FileNotFoundException;
    void writeNewMovieList(List<FileDto> fileDto, File file) throws IOException;
}